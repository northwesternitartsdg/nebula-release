<!DOCTYPE html>

<html ng-app="presto-app" ng-init="">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title><?php echo $title;?></title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">

  <!-- VIS -->
  <script type="text/javascript" src="bower_components/vis/dist/vis.js"></script>
  <!-- JQUERY -->
  <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- BOOTSTRAP -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/nu-bootstrap.css">
  <link rel="stylesheet" href="apps/<?php echo $app->id;?>/css/styles.css">
  <link rel="stylesheet" href="apps/<?php echo $app->id;?>/css/legend.css">
  <link rel="stylesheet" href="apps/<?php echo $app->id;?>/css/jplot.css">
  <link href="bower_components/vis/dist/vis.css" rel="stylesheet" type="text/css"/>
  <!-- ANGULAR -->
  <script type="text/javascript" src="bower_components/angular/angular.min.js"></script>
  <script type="text/javascript" src="bower_components/angular-cookies/angular-cookies.min.js"></script>
  <script src="bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>

  <!-- MISC -->
  <script src="bower_components/uri.js/src/URI.min.js"></script>
  <script type="text/javascript" src="js/lib/papaparse.js"></script>
  <script src="apps/<?php echo $app->id;?>/js-nocompress/params.js"></script>
  <script src="apps/<?php echo $app->id;?>/script.js"></script>
<link href="https://du11hjcvx0uqb.cloudfront.net/dist/brandable_css/ad711c6cf051c64652b8da7ba2240b46/new_styles_normal_contrast/bundles/tinymce-95b5e0c220.css" media="all" rel="stylesheet" type="text/css" />
  <script src="//tinymce.cachefly.net/4.3/tinymce.min.js"></script>
  <script src="js/lib/tinymce.min.js"></script>

</head>

<body>
<?php
$phpjs->other = new StdClass();
$phpjs->course_id = $_REQUEST['custom_canvas_course_id'];
//$phpjs->api_host = $_REQUEST['custom_canvas_api_domain'];
if( $devel ) {
  $phpjs->credentials->user = $app_config['app']['test_user'];
  $phpjs->credentials->uid = $app_config['app']['test_uid'];
}
?>

  <section class="page" ng-controller="PrestoApp as c" ng-init='php(<?php echo json_encode($phpjs)?>)'>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" rel="home" href="#"><?php echo $title;?></a>
        <p class="navbar-text">{{topics[topic_id].text}}</p>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
           {{corenav}}
        </ul>
        <div class="pull-right">
          <!--<p class="navbar-text" ng-show="graph_settings.group.enabled">{{group_name || "Release Candidate"}}</p>
         ng-hide="graph_settings.group.enabled"-->
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" data-toggle="dropdown" class="group dropdown-toggle">{{group_name || "Choose Group"}} <b class="caret"></b></a>
              <ul class="dropdown-menu dropdown-menu-right">
                  <li ng-repeat="group in group_list"><a href="#" ng-click="set_group(group)">{{group.name}}</a></li>
                  <li class="divider"></li>
                  <li><a href="#" ng-click="set_group(0)">Main Discussion</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div id="main-panel" class="container-fluid">
    <div class="legend-panel panel">
      <div class="legend-panel-heading panel-heading" ng-class="{'collapsed': legend_collapsed}" data-target="#legend-items,.panel-footer" ng-click="legend_collapsed=!legend_collapsed"> <!--data-toggle="collapse" -->
        Discussion Topics <span class="caret"></span>
      </div>
      <div id="legend-items" class="legend container-fluid collapse in" ng-hide="legend_collapsed">
        <legend-item topic="row" topicsdue="topics_due" ng-repeat="row in topics"></legend-item>
      </div>
      <div class="panel-footer collapse in" ng-hide="legend_collapsed">
        <button type="button" ng-class="{'btn-primary': rating_exists, 'btn-default': !rating_exists}" class="btn" ng-click="showRating(rating)" ng-disabled="!rating_exists">
          <span class="glyphicon glyphicon-check"></span> My Rating</button>
        <button type="button" class="btn btn-default" ng-click="show_help()">
          <span class="glyphicon glyphicon-question-sign"></span> Help</button>
      </div>
    </div>
    <div class="row">
      <div class="alert alert-danger" ng-show="error" role="alert">({{error_count}}) {{error_message}}</div>
      <div class="progress" style="margin: 7px 7px 7px 250px;" ng-hide="progress.percent == 100 || hide_progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="{{progress.percent}}" aria-valuemin="0" aria-valuemax="100" style="width: {{progress.percent}}%;">
          {{progress.percent}}%
        </div>
      </div>
      <div style="margin: 7px 7px 7px 250px; text-align: center;" ng-show="chart_done" ng-hide="!chart_done || stabilizing.percent == 100 || progress.percent != 100">
        <h3>Please wait, stabilizing {{stabilizing.percent | number:0}}%</h3>
      </div>
      <div style="margin: 7px 7px 7px 250px; text-align: center;" ng-show="chart_done && cdata.nodes.length == 0 ">
        <h3>There is nothing here :(</h3>
      </div>
        <network-graph data="cdata" done="chart_done" reply="reply" topicid="topic_id" stabilizing="stabilizing" topics="topics" params="network_params"></network-graph>
    </div>
   </div>
 </section>
</body>

</html>

<?php
/*
Copyright 2016 Northwestern University

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
*/
$config = parse_ini_file(dirname(__FILE__).'/../presto.ini', true);
$app_name = $config['app']['active'];
$app_config = parse_ini_file(dirname(__FILE__).'/../apps/'.$app_name.'/app.ini', true);
if( array_key_exists('code', $_GET) ) {
    $url = 'https://'.$app_config['app']['canvas_host'].'/login/oauth2/token';
    $data = array('grant_type' => 'authorization_code',
                  'client_id' => $app_config['oauth']['client_id'],
                  'redirect_uri' => $app_config['oauth']['redirect_uri'],
                  'code' => $_GET['code'],
                  'client_secret' => $app_config['oauth']['client_secret']);

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if( $result !== FALSE ) {
        $code = json_decode($result);
        setcookie('access_token', $code->access_token, time() + $code->expires_in - 30);
        setcookie('refresh_token', $code->refresh_token, time()+60*60*24*30);
    }
} else if ( array_key_exists('refresh_token', $_COOKIE) ) {
    $url = 'https://'.$app_config['app']['canvas_host'].'/login/oauth2/token';
    $data = array('grant_type' => 'refresh_token',
                  'client_id' => $app_config['oauth']['client_id'],
                  'redirect_uri' => $app_config['oauth']['redirect_uri'],
                  'refresh_token' => $_COOKIE['refresh_token'],
                  'client_secret' => $app_config['oauth']['client_secret']);

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if( $result !== FALSE ) {
        $code = json_decode($result);
        setcookie('access_token', $code->access_token, time() + $code->expires_in - 30);
    } else {
        setcookie('refresh_token', "", 1);
        echo("error");
        header("Refresh:0");
    }
} else {
    $host = str_replace(':8080', '', $_SERVER['HTTP_HOST']);
    $cookiedomain = ($_SERVER['HTTP_HOST'] != 'localhost') ? ".$host" : false;
    $this_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$host$_SERVER[REQUEST_URI]?post_oa=1";
    setcookie('oauth_source', $this_url, time()+3600, '/', $cookiedomain, false);
    //if( !$devel ) {
        header('Location: https://'.$app_config['app']['canvas_host'].'/login/oauth2/auth?client_id='.$app_config['oauth']['client_id'].'&response_type=code&redirect_uri='.$app_config['oauth']['redirect_uri']);
    //}
}
?>
